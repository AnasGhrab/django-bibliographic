from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import NoticeViewSet, Bibliographie, NoticeDetailView, BibeditList, NoticeTexViewSet

router = DefaultRouter()
router.register(r'notices', NoticeViewSet)
router.register(r'tex', NoticeTexViewSet, basename='notice-tex')

urlpatterns = [
    path('', Bibliographie.as_view()),
    path('api/', include(router.urls)),
    path('notice/<int:pk>/', NoticeDetailView.as_view(), name='notice-detail'),
    path('textes_edites/', BibeditList.as_view()),
]
