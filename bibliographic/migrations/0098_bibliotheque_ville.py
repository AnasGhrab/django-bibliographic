# Generated by Django 4.0.1 on 2022-05-02 14:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bibliographic', '0097_remove_bibliotheque_ville'),
    ]

    operations = [
        migrations.AddField(
            model_name='bibliotheque',
            name='ville',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='bibliographic.ville'),
        ),
    ]
