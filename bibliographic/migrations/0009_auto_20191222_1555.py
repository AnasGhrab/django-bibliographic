# Generated by Django 2.2.7 on 2019-12-22 14:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bibliographic', '0008_auto_20191221_1600'),
    ]

    operations = [
        migrations.AlterField(
            model_name='responsabilite',
            name='notice',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='notice', to='bibliographic.Notice', verbose_name='الجذاذة'),
        ),
    ]
