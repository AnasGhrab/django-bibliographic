# Generated by Django 3.1.4 on 2020-12-12 10:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bibliographic', '0068_auto_20201212_1016'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='notice',
            name='lang',
        ),
        migrations.RemoveField(
            model_name='notice',
            name='nb_volumes',
        ),
    ]
