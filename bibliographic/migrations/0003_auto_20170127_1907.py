# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-01-27 19:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bibliographic', '0002_notice_lang'),
    ]

    operations = [
        migrations.AddField(
            model_name='notice',
            name='pages',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='Pages dans la revue'),
        ),
        migrations.AddField(
            model_name='notice',
            name='volume',
            field=models.PositiveIntegerField(blank=True, null=True, verbose_name='Volume dans la revue'),
        ),
    ]
