# Generated by Django 3.0.6 on 2020-07-13 06:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bibliographic', '0042_auto_20200713_0748'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='editeur',
            name='ville_delete',
        ),
        migrations.AddField(
            model_name='editeur',
            name='ville2',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='bibliographic.Ville'),
        ),
        migrations.AlterField(
            model_name='editeur',
            name='ville',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='البلدة'),
        ),
    ]
