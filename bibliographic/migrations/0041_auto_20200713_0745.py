# Generated by Django 3.0.6 on 2020-07-13 06:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bibliographic', '0040_auto_20200713_0741'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='editeur',
            options={'managed': True, 'verbose_name': 'النّاشر', 'verbose_name_plural': 'النّاشرون'},
        ),
    ]
