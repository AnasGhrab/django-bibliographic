# Generated by Django 3.1 on 2020-10-02 03:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bibliographic', '0050_auto_20201002_0316'),
    ]

    operations = [
        migrations.AddField(
            model_name='autorite',
            name='defini',
            field=models.BooleanField(default=False, help_text="Concerne la langue arabe : rajouter l'article défini", verbose_name='article défini'),
        ),
    ]
