# Generated by Django 2.2.9 on 2020-04-10 17:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bibliographic', '0028_auto_20200305_0938'),
    ]

    operations = [
        migrations.RenameField(
            model_name='autorite',
            old_name='renom_trans',
            new_name='prenom_trans',
        ),
    ]
