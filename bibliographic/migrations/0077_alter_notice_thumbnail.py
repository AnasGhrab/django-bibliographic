# Generated by Django 4.0.1 on 2022-01-21 19:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bibliographic', '0076_alter_notice_options_alter_responsabilite_options_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notice',
            name='thumbnail',
            field=models.CharField(blank=True, max_length=2000, null=True, verbose_name='Image réduite'),
        ),
    ]
