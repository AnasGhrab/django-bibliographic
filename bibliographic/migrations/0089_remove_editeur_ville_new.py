# Generated by Django 4.0.1 on 2022-04-27 08:27

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bibliographic', '0088_editeur_ville_new'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='editeur',
            name='ville_new',
        ),
    ]
