# Generated by Django 3.1 on 2020-10-20 09:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bibliographic', '0061_auto_20201020_1027'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='notice',
            name='lang',
        ),
    ]
