from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

class BibliographicConfig(AppConfig):
    name = 'bibliographic'
    default_auto_field = 'django.db.models.BigAutoField'
    verbose_name = _('Gestion bibilographique')
#    label = _('Gestion bibliographique')

    def ready(self):
        import bibliographic.models.signals
