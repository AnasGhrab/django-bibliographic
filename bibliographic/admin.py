#!/usr/local/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.contrib import admin
from bibliographic.models.models import *
import bibliographic.models.pays as BP
from .models.bibliotheque import *
from django.utils.translation import gettext as _
from django.forms import CheckboxSelectMultiple

class BasiclogAdmin(admin.ModelAdmin):
	exclude = ['created_by', 'modified_by']
	def save_model(self, request, obj, form, change):
	   if not obj.pk:
		   obj.created_by = request.user
	   obj.modified_by = request.user
	   super().save_model(request, obj, form, change)

class ResponsabiliteInline(admin.StackedInline):
    model = Responsabilite
    extra = 1
    autocomplete_fields = ['autorite']


class NoticeAdmin(BasiclogAdmin):
        list_display = ['bibid', 'titre', 'annee', 'type_notice', 'created_by', 'created_on', 'modified_by', 'modified_on']
        list_filter = ['type_notice']
        search_fields = ['titre', 'bibid']
        inlines = [ ResponsabiliteInline, ]
        autocomplete_fields = ['editeur']

class EditeurAdmin(BasiclogAdmin):
	list_display = ['editeur', 'ville']
	search_fields = ['editeur']

class TypeNoticeAdmin(admin.ModelAdmin):
    def has_module_permission(self, request):
        return False

class ThemeAdmin(admin.ModelAdmin):
    def has_module_permission(self, request):
        return False

class FonctionAdmin(admin.ModelAdmin):
    def has_module_permission(self, request):
        return False

class AutoriteAdmin(admin.ModelAdmin):
	list_display = ['prenom', 'nom', 'prenom_trans', 'nom_trans']
	search_fields = ['prenom', 'nom', 'prenom_trans', 'nom_trans']

class PeriodiqueAdmin(BasiclogAdmin):
	list_display = ['nom', 'abbrev', 'nom_trans', 'editeur']
	search_fields = ['prenom', 'nom']

class ResponsabiliteAdmin(admin.ModelAdmin):
	list_display = ['autorite', 'fonction', 'notice']
	list_filter = ['fonction']
	search_fields = ['notice__titre', 'autorite__nom', 'autorite__prenom', 'notice__editeur__editeur']
	#search_fields = ['notice__titre', 'autorite__nom', 'autorite__prenom', 'notice__editeur']

	def has_module_permission(self, request):
		return False

class VilleAdmin(admin.ModelAdmin):
    def has_module_permission(self, request):
        return False

class PaysAdmin(admin.ModelAdmin):
    def has_module_permission(self, request):
        return False

admin.site.site_header = _('Modification des données')

admin.site.register(TypeNotice, TypeNoticeAdmin)
admin.site.register(Notice,NoticeAdmin)
admin.site.register(Fonction, FonctionAdmin)
admin.site.register(Autorite, AutoriteAdmin)
admin.site.register(Responsabilite,ResponsabiliteAdmin)
admin.site.register(Editeur, EditeurAdmin)
admin.site.register(Periodique, PeriodiqueAdmin)
admin.site.register(Theme, ThemeAdmin)
admin.site.register(BP.Ville, VilleAdmin)
admin.site.register(BP.Pays, PaysAdmin)
admin.site.register(Bibliotheque)
admin.site.register(Localisation)
