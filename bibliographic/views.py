from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView, DetailView

from bibliographic.models.models import *

from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from bibliographic.models.models import Notice
from .serializers import *

class NoticeViewSet(viewsets.ModelViewSet):
    queryset = Notice.objects.all().select_related('editeur').prefetch_related('responsabilite_set')
    serializer_class = NoticeSerializer

class NoticeTexViewSet(viewsets.ModelViewSet):
    queryset = Notice.objects.all().select_related('editeur').prefetch_related('responsabilite_set')
    serializer_class = NoticeSerializer

    @action(detail=True, methods=['get'])
    def bibtex(self, request, pk=None):
        notice = self.get_object()
        serializer = BibTeXSerializer(notice)
        return Response(serializer.data)

#------------------------------------------------------------

class Bibliographie(ListView):
    # All the content of the database
    model = Notice

class NoticeDetailView(DetailView):
    model = Notice

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

class BibeditList(ListView):
    model = Notice
    template_name = "web/textes_edites_list.html"

    def get_queryset(self):
        return Notice.objects.filter(themes__id__exact=5)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['responsabilites'] = Responsabilite.objects.filter(notice__in = self.object_list).order_by('fonction')
        context['notices_ar'] = Notice.objects.filter(themes__id__exact=5).filter(lang_notice = 'ar')
        context['notices_nar'] = Notice.objects.filter(themes__id__exact=5).exclude(lang_notice = 'ar')
        return context
