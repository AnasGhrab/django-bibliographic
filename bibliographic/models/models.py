# models/models.py
from django.utils.translation import gettext as _
from django.db import models
from datetime import datetime
from django.conf import settings
from bibliographic.models.pays import Ville
from PIL import Image

class Basetable(models.Model):
    created_on = models.DateTimeField(verbose_name=_('Date du rajout'), auto_now_add=True, null=True, blank=True)
    modified_on = models.DateTimeField(verbose_name=_('Dernière modification'), auto_now=True, null=True, blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('Rajouté par'), related_name='%(class)s_created_by', null=True, blank=True, on_delete=models.SET_NULL)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('Modifié par'), related_name='%(class)s_modified_by', null=True, blank=True, on_delete=models.SET_NULL)

    class Meta:
        abstract = True

class Theme(models.Model):
    theme = models.CharField(_('theme'), max_length=50)

    class Meta:
        managed = True
        db_table = 'bibliographic_themes'
        verbose_name = _('Theme')
        verbose_name_plural = _('Themes')

    def __str__(self):
        return '%s' % (self.theme)

class Fonction(models.Model):
    intitule = models.CharField(_('fonction'), max_length=50, null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'bibliographic_fonctions'
        verbose_name = _('Fonction')
        verbose_name_plural = _('Fonctions')

    def __str__(self):
        return '%s' % (self.intitule) or ''

class Autorite(models.Model):
    prenom = models.CharField(_('prenom'), max_length=50, null=True, blank=True)
    prenom_trans = models.CharField(_('prenom trans'), max_length=50, null=True, blank=True)
    prenom_gen = models.CharField(_('prenom generi'), max_length=50, null=True, blank=True)
    nom = models.CharField(_('nom'), max_length=50, null=True, blank=True, help_text=_('Sans l\'article défini'))
    nom_trans = models.CharField(_('nom translitt'), max_length=50, null=True, blank=True)
    nom_gen = models.CharField(_('nom generi'), max_length=50, null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'bibliographic_autorites'
        verbose_name = _('Autorité')
        verbose_name_plural = _('02. Autorités')
        ordering = ['prenom', 'nom']

    def __str__(self):
        return '%s %s' % (self.prenom, self.nom) or ''

class Editeur(models.Model):
    editeur = models.CharField(_('Éditeur'), max_length=200, null=True, blank=True)
    editeur_trans = models.CharField(_('Éditeur translittéré'), max_length=50, null=True, blank=True)
    ville = models.CharField(_('Ville'), max_length=20, null=True, blank=True)
    pays = models.CharField(_('Pays'), max_length=20, null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'bibliographic_editeurs'
        verbose_name = _('Éditeur')
        verbose_name_plural = _('04. Éditeurs')
        ordering = ['ville', 'editeur']

    def __str__(self):
        return '%s' % (self.editeur) or ''

class Periodique(Basetable):
    nom = models.CharField(_('Titre du  périodique'), max_length=300, null=True, blank=True)
    abbrev = models.CharField(_('Titre abbrégé du période'), max_length=30, null=True, blank=True)
    nom_trans = models.CharField(_('Titre période translittéré'), max_length=200, null=True, blank=True)
    editeur = models.ForeignKey(Editeur, verbose_name=_('Éditeur'), null=True, blank=True, on_delete=models.SET_NULL)

    class Meta:
        managed = True
        db_table = 'bibliographic_periodiques'
        verbose_name = _('Périodique')
        verbose_name_plural = _('03. Périodiques')

    def __str__(self):
        return '%s' % (self.nom) or ''

class TypeNotice(models.Model):
    type_notice = models.CharField(_('Type notice'), max_length=50, null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'bibliographic_type_notice'
        verbose_name = _('Type de notice')
        verbose_name_plural = _('Types de notice')

    def __str__(self):
        return '%s' % (self.type_notice) or ''

class Notice(Basetable):
    LANGUES = (
        ('ar', _('arabe')),
        ('fr', _('français')),
        ('en', _('anglais')),
        ('heb', _('hebreu')),
        ('it', _('italien')),
        ('de', _('allemand')),
        ('per', _('persan')),
        ('tur', _('turc')),
    )

    bibid = models.CharField(max_length=100, unique=True, null=True, blank=True, verbose_name=_('BibID'))
    type_notice = models.ForeignKey(TypeNotice, verbose_name=_('Type de notice'), null=True, blank=False, on_delete=models.SET_NULL)
    lang_doc = models.CharField(_('Langue'), max_length=3, choices=LANGUES, default="ar", help_text=_('Langue principale du document'))
    lang_notice = models.CharField(_('Langue'), max_length=3, choices=LANGUES, default="ar", help_text=_('Langue de la notice'))
    titre = models.CharField(_('Titre'), max_length=400, null=True, blank=False)
    nb_volumes = models.PositiveIntegerField(null=True, blank=True)
    titre_ouvrage = models.CharField(_('In : Titre de l\'ouvrage'), max_length=300, null=True, blank=True)
    edition_critique = models.BooleanField(default=False)
    traduction = models.BooleanField(default=False)
    isbn = models.CharField(_('ISBN'), max_length=20, null=True, blank=True)
    editeur = models.ForeignKey(Editeur, verbose_name=_('Éditeur'), null=True, blank=True, on_delete=models.SET_NULL)
    serie = models.CharField(verbose_name=_('Série'), max_length=100, null=True, blank=True)
    num_edition = models.PositiveIntegerField(verbose_name=_('Édition'), null=True, blank=True)
    num_in_serie = models.IntegerField(verbose_name=_('Numéro dans la série'), null=True, blank=True)
    periodique = models.ForeignKey(Periodique, verbose_name=_('Périodique'), null=True, blank=True, on_delete=models.SET_NULL)
    annee = models.PositiveIntegerField(_('Année de publication'), null=True, blank=True)
    mois = models.PositiveIntegerField(_('Mois de publication'), null=True, blank=True)
    volume = models.CharField(_('Volume dans la revue'), max_length=50, null=True, blank=True)
    numero = models.PositiveIntegerField(_('Numéro dans la publication'), null=True, blank=True)
    pages = models.CharField(_('Pages dans la revue'), max_length=20, null=True, blank=True)
    remarques = models.TextField(_('Remarques'), null=True, blank=True)
    fichier = models.FileField(verbose_name=_('Fichier numérique (pdf)'), upload_to='pdfs', null=True, blank=True)
    image = models.ImageField(verbose_name=_('Image de couverture'), upload_to='images', null=True, blank=True)
    #lang = models.CharField(verbose_name=_('Langue'), max_length=3, blank=True, default='ar')
    url = models.URLField(verbose_name=_('Lien Web'), null=True, blank=True)
    data_consultation = models.DateField(verbose_name=_('Date de consultation'), null=True, blank=True)

    def save(self, *args, **kwargs):
        super(Notice, self).save(*args, **kwargs)
        if self.image.name:
            if not self.make_thumbnail():
                raise Exception('Could not create thumbnail - is the file type valid?')

    def make_thumbnail(self):
        import os
        from django.core.files.base import ContentFile
        from django.core.files.storage import default_storage as storage
        from PIL import Image

        THUMB_SIZE = (440, 600)

        with open(settings.MEDIA_ROOT + '/' + self.image.name, 'rb') as fh:
            image = Image.open(fh)
            image.thumbnail(THUMB_SIZE)

            thumb_name, thumb_extension = os.path.splitext(self.image.name)
            thumb_name = os.path.basename(self.image.name)
            thumb_extension = thumb_extension.lower()
            thumb_filename = 'thumb_' + thumb_name

            if thumb_extension in ['.jpg', '.jpeg']:
                FTYPE = 'JPEG'
            elif thumb_extension == '.gif':
                FTYPE = 'GIF'
            elif thumb_extension == '.png':
                FTYPE = 'PNG'
            else:
                return False

            image.save(settings.MEDIA_ROOT + '/images/thumbnails/' + thumb_filename, FTYPE)

            return True

    class Meta:
        managed = True
        db_table = 'bibliographic_notice'
        verbose_name = _('Notice')
        verbose_name_plural = _('01. Notices')
        ordering = ['annee', 'mois']

    def __str__(self):
        return '%s : %s' % (self.annee, self.titre) or (self.annee, self.titre_ouvrage) or u''

class Responsabilite(models.Model):
    notice = models.ForeignKey(Notice, verbose_name=_('notice'), null=True, blank=True, on_delete=models.SET_NULL)
    autorite = models.ForeignKey(Autorite, verbose_name=_('autorite'), null=True, blank=True, on_delete=models.SET_NULL)
    fonction = models.ForeignKey(Fonction, verbose_name=_('fonction'), null=True, blank=True, on_delete=models.SET_NULL)

    class Meta:
        managed = True
        db_table = 'bibliographic_responsabilites'
        verbose_name = _('Mention de responsabilité')
        verbose_name_plural = _('Mentions de responsabilité')

    def __str__(self):
        return '%s (%s : %s)' % (self.notice.titre, self.autorite, self.fonction)
