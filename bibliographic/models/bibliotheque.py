#!/usr/local/bin/python
  # -*- coding: utf-8 -*-

from django.utils.translation import gettext as _
from django.db import models
from .models import *
from .pays import *

class Bibliotheque(models.Model):
    intitule_ar = models.CharField(_('Nom'),max_length=100)
    naan = models.CharField(_('Identifiant ARK-NAAN'), max_length=100, null=True, blank=True, help_text=_('https://arks.org/about/ark-naans-and-systems'))
    ville = models.ForeignKey(Ville, null=True, blank=True, on_delete = models.SET_NULL)

    class Meta:
        managed = True
        db_table = 'd_bibliotheques'
        verbose_name = _('Bibliothèque')
        verbose_name_plural = _('x. 01. Bibliothèques')
        ordering = ('intitule_ar',)

    def __str__(self):
        return '%s' % (self.intitule_ar)

class Localisation(models.Model):
    notice = models.ForeignKey(Notice, null=True, on_delete = models.SET_NULL)
    bibliotheque = models.ForeignKey(Bibliotheque, null=True, on_delete = models.SET_NULL)
    cote = models.CharField(max_length=100, null=True, blank=True)
    qualifier = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'd_localisation'
        verbose_name = _('Localisation')
        verbose_name_plural = _('x. 02. Localisations')

    def __str__(self):
        return '%s %s' % (self.notice, self.bibliotheque)
