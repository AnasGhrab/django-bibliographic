import random
import string
from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Notice, Responsabilite

def generate_random_string(length=4):
    """Generate a random string of fixed length."""
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))

@receiver(post_save, sender=Responsabilite)
def update_bibid(sender, instance, **kwargs):
    notice = instance.notice
    if notice:
        responsabilite = Responsabilite.objects.filter(notice=notice).first()
        if responsabilite and responsabilite.autorite and notice.annee:
            # Start with the basic bibid
            base_bibid = f"{responsabilite.autorite.prenom}{notice.annee}"
            bibid = base_bibid
            
            # Check if the bibid already exists in another Notice
            while Notice.objects.filter(bibid=bibid).exists():
                # Modify the bibid by appending a random string
                random_suffix = generate_random_string()
                bibid = f"{base_bibid}_{random_suffix}"
            
            # Update the notice with the unique bibid
            notice.bibid = bibid
            notice.save()
