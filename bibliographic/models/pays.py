#!/usr/local/bin/python
  # -*- coding: utf-8 -*-

from django.utils.translation import gettext as _
from django.db import models

class Pays(models.Model):
    nom_ar = models.CharField(_('nom_pays_ar'),max_length=40,null=True,blank=True,help_text=_('Nom pays ar'))
    determination = models.BooleanField(default=False)
    nom_fr = models.CharField(_('nom_pays_fr'),max_length=40,null=True,blank=True,help_text=_('Nom pays fr'))
    nom_en = models.CharField(_('nom_pays_en'),max_length=40,null=True,blank=True,help_text=_('Nom pays en'))
    code = models.CharField(_('code_pays'),max_length=3,null=True,blank=True,help_text=_('Nom pays en'))

    class Meta:
        managed = True
        db_table = 'bibliographic_pays'
        verbose_name = _('Pays')
        verbose_name_plural = _('Pays')
        ordering = ['nom_ar']

    def __str__(self):
        return self.nom_ar

class Ville(models.Model):
    nom_ar = models.CharField(_('nom_ville'),max_length=20,null=True,blank=True,help_text=_('Nom de la ville'))
    nom_fr = models.CharField(_('nom_ville_fr'),max_length=20,null=True,blank=True,help_text=_('Nom de la ville - fr'))
    nom_en = models.CharField(_('nom_ville_en'),max_length=20,null=True,blank=True,help_text=_('Nom de la ville - en'))
    pays = models.ForeignKey(Pays, null=True,blank=True,help_text=_('Pays'), on_delete=models.SET_NULL)
    #lat = models.FloatField(_('lattitude'),null=True,blank=True)
    #longi = models.FloatField(_('longitude'),null=True,blank=True)

    class Meta:
        managed = True
        db_table = 'bibliographic_ville'
        verbose_name = _('Ville')
        verbose_name_plural = _('Villes')
        ordering = ['nom_ar']

    def __str__(self):
        return '%s (%s)' % (self.nom_ar, self.pays)
