��    N      �  k   �      �     �  	   �     �     �     �     �       	        )     A     F     Z     r     y     �     �     �     �     �     	          -     B     W     c     o     {     �     �     �     �     �     �  	   �     �     	     	     	     	     "	     >	     T	     r	     �	     �	     �	     �	     �	     �	     �	     �	     �	  	   �	     �	  	   �	     �	     
     
  
   
     
     (
     4
     @
  	   L
     V
     c
     p
     w
     ~
     �
     �
     �
     �
     �
     �
     �
     �
  �  �
     �     �  
   �     �     �  '   �          "  )   1  $   [     �     �     �     �     �     �  !   
     ,     H     b     v     �     �  
   �     �     �  
     
     "        ;  
   L     W     j     �  "   �     �     �     �     �  (   �       C   7     {     �     �     �  
   �     �     �               +     :  
   L     W     f     s     �     �  2   �  
   �     �     �               ?  
   [     f     u     |  *   �     �     �     �     �     �  2   
     B   :   A       .           2          C   J   =       
           M   9                           3          L          <   N          E   +   I       )   F   ,       8       6   -          &   7   1      5                           0   *      (       	          #              H      /              D   $   4      ;      !      @   "                       G       %      ?       K   >   '       Année de publication Autorité Bibliothèque Date du rajout Dernière modification Fichier numérique (pdf) Fonction Fonctions Gestion bibilographique ISBN Image de couverture In : Titre de l'ouvrage Langue Langue de la notice Langue principale du document Mention de responsabilité Mentions de responsabilité Modification des données Modifié par Mois de publication Nom de la ville Nom de la ville - en Nom de la ville - fr Nom pays ar Nom pays en Nom pays fr Notice Numéro dans la publication Numéro dans la série Pages dans la revue Pays Périodique Rajouté par Remarques Sans l'article défini Série Theme Themes Titre Titre abbrégé du période Titre du  périodique Titre période translittéré Type de notice Type notice Types de notice Ville Villes Volume dans la revue allemand anglais arabe autorite code_pays fonction français hebreu italien nom nom generi nom translitt nom_pays_ar nom_pays_en nom_pays_fr nom_ville nom_ville_en nom_ville_fr notice persan prenom prenom generi prenom trans theme turc x. 01. Bibliothèques x. 02. Localisations Éditeur Éditeur translittéré Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
 تاريخ النّشر المسؤول مكتبة تاريخ الإضافة آخر إضافة الملفّ الرقمي (ب. د. ف.) الوظيفة الوظائف التصرّف البيبليوغرافي رقم التعريف الدّولي صورة الغلاف ضمن الكتاب اللّغة لغةالجذاذة اللّغة الأساسيّة بيان المسؤوليّة بيانات المسؤوليّة تغيير البيانات قام بالتّغيير شهر النّشر المدينة المدينة (انجليزي) المدينية (فرنسي) البلد البلد (إتجليزي) البلد (فرنسي) جذاذة العدد العدد ضمن المجموعة الصّفحات البلد الدّوريّة قام بالإضافة ملاحظات اللّقب دون التعريف المجوعة المجال المجالات العنوان مختصر عنوان الدّوريّة عنوان الدّوريّة عنوان الدّوريّة بالأحرف اللّاتينيّة نوعيّة الجذاذة نوعيّة الجذاذة أنواع الجذاذات المدينة المدن المجموعة ألمانيّة إنجليزيّة عربيّة المسؤول رمز البلد وظيفة فرنسيّة عبريّة إيطاليّة اللّقب اللّقب اللّقب بالأحرف اللّاتينيّة البلد البلد (إنجليزي) البلد (فرنسي) المدينة المدينة (إنجليزي) المدينة (فرنسي) جذاذة فارسيّة اسم اسم الشهرة اسم بالأحرف اللاتينيّة مجال عثمانيّة المكتبات الاحتضانات النّاشر الناشر بالأحرف اللّاتينيّة 