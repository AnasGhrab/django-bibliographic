import os
from django.conf import settings
from rest_framework import serializers
from bibliographic.models.models import Notice, Editeur, Responsabilite, Autorite, Fonction

class EditeurSerializer(serializers.ModelSerializer):
    class Meta:
        model = Editeur
        fields = ['editeur']

class AutoriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Autorite
        fields = ['prenom', 'nom']

class FonctionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fonction
        fields = ['intitule']

class ResponsabiliteSerializer(serializers.ModelSerializer):
    autorite = AutoriteSerializer(read_only=True)
    fonction = FonctionSerializer(read_only=True)

    class Meta:
        model = Responsabilite
        fields = ['autorite', 'fonction']

class NoticeSerializer(serializers.ModelSerializer):
    editeur = EditeurSerializer(read_only=True)
    responsabilites = ResponsabiliteSerializer(source='responsabilite_set', many=True, read_only=True)
    thumbnail_url = serializers.SerializerMethodField()

    class Meta:
        model = Notice
        fields = '__all__'

    def get_thumbnail_url(self, obj):
        if obj.image:
            return os.path.join(settings.MEDIA_URL, 'images/thumbnails', 'thumb_' + os.path.basename(obj.image.url))
        return None

class BibTeXSerializer(serializers.BaseSerializer):
    def to_representation(self, obj):
        entry_type = "article" if obj.periodique else "book"
        author_list = Responsabilite.objects.filter(notice=obj).select_related('autorite').values_list('autorite__nom', 'autorite__prenom')
        authors = " and ".join(["{}, {}".format(author[0], author[1]) for author in author_list])

        bibtex_entry = f"@{entry_type}{{{obj.bibid},\n"
        bibtex_entry += f"  author = {{{authors}}},\n"
        bibtex_entry += f"  title = {{{obj.titre}}},\n"
        bibtex_entry += f"  year = {{{obj.annee}}},\n"
        if obj.editeur:
            bibtex_entry += f"  publisher = {{{obj.editeur.editeur}}},\n"
        if obj.periodique:
            bibtex_entry += f"  journal = {{{obj.periodique.nom}}},\n"
        bibtex_entry += f"}}"

        return bibtex_entry