====================
django-bibliographic
====================


Quick start
-----------

1. Add "bibliographic" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'bibliographic',
    ]

2. Include the polls URLconf in your project urls.py like this::

    path('bibliographic/', include('bibliographic.urls')),

3. Run ``python manage.py migrate`` to create the polls models.

4. Start the development server and visit http://127.0.0.1:8000/admin/
   to create a poll (you'll need the Admin app enabled).

5. Visit http://127.0.0.1:8000/bibliographic/ to the bibliographic data.

Contact
=======

Homepage: http://anas.ghrab.tn

Email:

 * Anas Ghrab <anas.ghrab@gmail.com>

License
=======

.. image:: img/by-nc-sa1.png
   :height: 40
   :target: https://creativecommons.org/licenses/by-nc-sa/3.0/
